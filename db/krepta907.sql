-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 07, 2017 at 02:32 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krepta`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_friend`
--

CREATE TABLE `tb_friend` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `status` int(2) NOT NULL COMMENT '1:accept, -1:reject'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invite`
--

CREATE TABLE `tb_invite` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `status` int(2) NOT NULL COMMENT '0:sent, 1:received, 2:accept',
  `invite_condition` text COLLATE utf8_unicode_ci NOT NULL,
  `reply_condition` text COLLATE utf8_unicode_ci NOT NULL,
  `is_invited` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_invite`
--

INSERT INTO `tb_invite` (`id`, `user_id`, `sender_id`, `status`, `invite_condition`, `reply_condition`, `is_invited`) VALUES
(1, 15, 14, 2, 'wash my room', 'table I', 0),
(5, 15, 13, 0, 'wash my room', '', 0),
(6, 15, 11, 0, 'wash my room', '', 0),
(27, 11, 13, 2, 'chech rating', 'ok,good', 1),
(28, 11, 14, 0, 'chech rating', '', 0),
(29, 11, 15, 0, 'chech rating', '', 0),
(30, 11, 16, 0, 'chech rating', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_message`
--

CREATE TABLE `tb_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `type` int(2) NOT NULL COMMENT '0:text, 1:image, 2:video',
  `read_status` int(2) NOT NULL DEFAULT '0',
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_message`
--

INSERT INTO `tb_message` (`id`, `user_id`, `sender_id`, `message`, `type`, `read_status`, `reg_date`) VALUES
(1, 15, 14, 'http://18.220.26.10/uploadfiles/files/15_file15042618880.m4v', 2, 0, '2017-09-01 10:31:28'),
(7, 15, 14, 'http://18.220.26.10/uploadfiles/files/15_file15045279528.png', 1, 0, '2017-09-04 12:25:52'),
(8, 14, 15, 'http://18.220.26.10/uploadfiles/files/14_file15045281381.png', 1, 0, '2017-09-04 12:28:58'),
(9, 14, 15, 'http://18.220.26.10/uploadfiles/files/14_file15045281390.png', 1, 0, '2017-09-04 12:28:59'),
(11, 15, 14, 'http://18.220.26.10/uploadfiles/files/15_file15047101295.m4v', 2, 0, '2017-09-06 15:02:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read_status` int(2) NOT NULL,
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_notification`
--

INSERT INTO `tb_notification` (`id`, `user_id`, `sender_id`, `content`, `read_status`, `reg_date`) VALUES
(1, 15, 14, 'You are ready to play', 1, '2017-09-01 10:31:10'),
(3, 15, 14, 'You are ready to play', 1, '2017-09-04 12:25:29'),
(5, 15, 14, 'You are ready to play', 0, '2017-09-06 15:01:44'),
(6, 11, 13, 'You are ready to play', 0, '2017-09-07 01:39:21'),
(7, 11, 13, 'You are rated!', 0, '2017-09-07 01:40:39'),
(8, 11, 13, 'You are ready to play', 0, '2017-09-07 01:52:38'),
(9, 11, 13, 'You are rated!', 0, '2017-09-07 02:00:16'),
(10, 11, 13, 'You are ready to play', 0, '2017-09-07 02:06:30'),
(11, 11, 13, 'You are rated!', 0, '2017-09-07 02:08:34'),
(12, 11, 13, 'You are ready to play', 0, '2017-09-07 02:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_play`
--

CREATE TABLE `tb_play` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `status` int(2) NOT NULL COMMENT '0:onlive, 1:moven to chat',
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_play`
--

INSERT INTO `tb_play` (`id`, `user_id`, `sender_id`, `status`, `reg_date`) VALUES
(1, 15, 14, 1, '2017-09-01 10:31:10'),
(2, 14, 15, 1, '2017-09-01 10:31:10'),
(15, 11, 13, 0, '2017-09-07 02:06:30'),
(16, 13, 11, 0, '2017-09-07 02:06:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rate`
--

CREATE TABLE `tb_rate` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `rate` float(2,1) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_rate`
--

INSERT INTO `tb_rate` (`id`, `challenge_id`, `user_id`, `sender_id`, `rate`, `comment`, `reg_date`) VALUES
(1, 1, 14, 15, 0.0, 'bad1', '2017-09-01 10:39:07'),
(2, 1, 15, 14, 0.0, 'bad1', '2017-09-01 10:41:27'),
(19, 1, 11, 13, 0.0, '', '2017-09-07 02:06:30'),
(20, 1, 13, 11, 0.0, '', '2017-09-07 02:06:30'),
(21, 2, 11, 13, 0.0, '', '2017-09-07 02:11:22'),
(22, 2, 13, 11, 0.0, '', '2017-09-07 02:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(3) NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `condition` text COLLATE utf8_unicode_ci NOT NULL,
  `status` text COLLATE utf8_unicode_ci NOT NULL,
  `device_token` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `name`, `username`, `email`, `age`, `gender`, `latitude`, `longitude`, `condition`, `status`, `device_token`, `reg_date`) VALUES
(11, 'YinFeng', 'star', 'mighty3star27@gmail.com', 28, 'female', 42.89432144, 129.56062317, 'chech rating', '', 'cF0xGvZ1zrc:APA91bF2mLlKTqt9ALTO5mdY-xe3L3jgshg26NOWRpQSrcz2I6kD8j_Q9EgPt_kexmH89zqMNs5BkUaiNS5KHqr-WJ59Ro5b0kbM5KM06-Ct8XuNiu81sjgePnm8hZZjCu8JZWS8Du95', '2017-08-30 12:42:38'),
(13, 'GuangZhun', 'gzz', 'guangzhun1212@gmail.com', 29, 'male', 37.78583527, -122.40641785, '', '', '', '2017-08-30 14:46:48'),
(14, 'Grant', 'granting', 'mauryn1@yahoo.com', 18, 'male', 51.53183746, -0.07449852, '', '', '', '2017-08-31 13:12:35'),
(15, 'Mach', 'Chris', '1563351963716691@facebook.com', 24, 'male', 51.51488113, -0.07427406, 'wash my room', '', 'ft4PiRM2htE:APA91bGVM5GLXRxyB3TVQAEZfm-J6Nk3vO1-jNDWz9dwcW9WSQPxfwzVI_lgjG7YIdQNtp_iWZ_flTP_Ox40uj9uEjgQ14aCp7nxDlRjcUya6neNCybzUOVoq4t_1pwHabANMrWg7EAW', '2017-09-01 10:23:14'),
(16, 'Zhandong', 'zhandong', 'zhandong0217@gmail.com', 25, 'male', 42.89436340, 129.56062317, 'test rating', '', 'dMLcBs80LDA:APA91bGiNl84GqdSbDilD_BQ3mm0qP2rf_pSPiJ7U9C4LVO9qbO9ZCYPDoqXKacoNlFD5iG35I_i2v2TIUPJIvXMNWsHlX1hFmHmMdEDbcOWfw99qQBV8hmXX8MekFBKwHB1_-5TZdpV', '2017-09-04 09:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_profile`
--

CREATE TABLE `tb_user_profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_user_profile`
--

INSERT INTO `tb_user_profile` (`id`, `user_id`, `photo_id`, `photo_url`) VALUES
(145, 11, 0, 'https://fb-s-a-a.akamaihd.net/h-ak-fbx/v/t1.0-9/17457726_408194672874914_341660173561201886_n.jpg?oh=aba583431d2b41987f748086ac8f51fe&oe=5A5E16BD&__gda__=1516261684_a4c3b7135e8ee5b8ddf0e68818723b02'),
(146, 11, 1, 'https://fb-s-a-a.akamaihd.net/h-ak-fbx/v/t31.0-8/s720x720/19054978_445224502505264_407256638927278158_o.jpg?oh=c6796064a81075668cf3793c0b36e46c&oe=5A2D5A06&__gda__=1512095382_df1603fa506506ad8797034143344735'),
(147, 11, 2, 'https://scontent.xx.fbcdn.net/v/t31.0-8/s720x720/20776554_477157795978601_5215689850668036355_o.jpg?oh=04fa2b0b6a5ca3d61cc6c8481b5a61d6&oe=5A5CFAD6'),
(148, 11, 3, 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/21077667_483177288709985_6960077386095158428_n.jpg?oh=98f6d3f3ae5e33794ac2b1221993dda7&oe=5A1930DD'),
(153, 13, 0, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/19702005_113900625896579_8688797838017105423_n.jpg?oh=85305162cf1bfef7f5e8697426388cfd&oe=5A57EEB3'),
(154, 13, 1, 'https://fb-s-d-a.akamaihd.net/h-ak-fbx/v/t31.0-8/s720x720/19955872_117142062239102_4620166925751875840_o.jpg?oh=a57060df3fda588233b18f8f3a9fd433&oe=5A6133DB&__gda__=1512408949_210f9867da5e8dc30a9e7d17febd7960'),
(155, 13, 2, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/19990173_117149988904976_942707688203576754_n.jpg?oh=46aa6c367e8e632dcf2a251ae19e8172&oe=5A5B8E3C'),
(156, 13, 3, 'http://18.220.26.10/uploadfiles/files/file15041043436.png'),
(157, 14, 0, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/11659424_10204415691179593_1693703471994408510_n.jpg?oh=91f835ccc4dc4f823d76e6cf35b1065b&oe=5A2C8383'),
(158, 14, 1, 'https://scontent.xx.fbcdn.net/v/t1.0-9/60234_1414704614877_6683188_n.jpg?oh=2b1715ca1d4185869c3c1f8cc54e30cc&oe=5A1A516A'),
(159, 14, 2, ''),
(160, 14, 3, ''),
(161, 15, 0, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/13335652_1127186060666619_1558169379403777759_n.jpg?oh=5c3f2d20fd7bf40cae286c74e3847864&oe=5A5D7CA5'),
(162, 15, 1, 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/10294294_1022672321117994_4059034826003656619_n.jpg?oh=01b5b89881eab37af656794b13a67551&oe=5A1D612A'),
(163, 15, 2, 'http://18.220.26.10/uploadfiles/files/file15042613795.png'),
(164, 15, 3, ''),
(165, 16, 0, 'https://fb-s-b-a.akamaihd.net/h-ak-fbx/v/t1.0-9/18447457_431946923836458_5918467668680573668_n.jpg?oh=daca07b78dc5e61ba535f0393fb7dd2b&oe=5A184FE0&__gda__=1515894061_6dd7bc507a27b42c61c1b09b835c1669'),
(166, 16, 1, 'https://scontent.xx.fbcdn.net/v/t1.0-9/15977047_370963676601450_7861298174854563922_n.jpg?oh=7d5653aa7ba2f4577b891b7ca751cdb8&oe=5A515AF4'),
(167, 16, 2, 'https://scontent.xx.fbcdn.net/v/t31.0-8/s720x720/16179500_378403299190821_1667551036059730007_o.jpg?oh=4ff220bedf84f5feda7a7a45f96460d8&oe=5A1B69E6'),
(168, 16, 3, 'https://fb-s-b-a.akamaihd.net/h-ak-fbx/v/t1.0-9/15442319_354392038258614_6433875485156062290_n.jpg?oh=9cbdcd06a1ff4783c2f9117083a0b51f&oe=5A57161D&__gda__=1511509968_b433735fc9375f5eb6dc417d6e0f7401');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_friend`
--
ALTER TABLE `tb_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_invite`
--
ALTER TABLE `tb_invite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_message`
--
ALTER TABLE `tb_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_play`
--
ALTER TABLE `tb_play`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_rate`
--
ALTER TABLE `tb_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user_profile`
--
ALTER TABLE `tb_user_profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_friend`
--
ALTER TABLE `tb_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_invite`
--
ALTER TABLE `tb_invite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_message`
--
ALTER TABLE `tb_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_play`
--
ALTER TABLE `tb_play`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_rate`
--
ALTER TABLE `tb_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_user_profile`
--
ALTER TABLE `tb_user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
