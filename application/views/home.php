<html>

    <head>
        <title>Krepta</title>
    </head>

    <body style="background-color: #b6e6a9;">
    
    <!--<img src="services.png">  -->

        <center>

            <h3 style="margin-top: 100px;"><font color="black" size="10">Privacy Policy</font></h3>
        </center>
            

<div class="col-lg-6 text">
<p>This privacy policy applies between you, the User of this Website and Krepta, the owner and provider of this Website. Krepta takes the privacy of your information very seriously. This privacy policy applies to our use of any and all Data collected by us or provided by
you in relation to your use of the Website. Please read this privacy policy carefully </p>

<h4>Definitions and interpretation</h4>

<p>1.    In this privacy policy, the following definitions are used</p>

<strong>Data</strong> collectively all information that you submit to Krepta via the Website. This definition                                                           incorporates, where applicable the definitions provided in the Data Protection Act 1998;
<br>
<strong>Krepta</strong> of 35 Kingsland Road, London, e2 8aa; 


<p>     
   2.<br>
  
In this privacy policy, unless the context requires a different interpretation: <br>
a. the singular includes the plural and vice versa;  <br>
b. references to sub-clauses, clauses, schedules or appendices are to sub-clauses, clauses, schedules or appendices of this privacy policy; Unless in <br>
c. a reference to a person includes firms, companies, government entities, trusts and partnerships<br>
d. "including" is understood to mean "including without limitation"<br>
e. reference to any statutory provision includes any modification or amendment of it<br>
f. the headings and sub-headings do not form part of this privacy policy.<br>
</p>

<p>
<b>Scope of this privacy policy</b><br><br>
3. This privacy policy applies only to the actions of Krepta and Users with respect to this Mobile app. It does not extend to any third party websites or apps that can be accessed from this Website including, but not limited to, any links we may provide to social media
</p>
<p>

<p>
<strong>Data collected</strong></br><br>

4. We may collect the following Data, which includes personal Data, from you:<br> <br>

a. Name<br>
b. Date of Birth;<br>
c. Gender;<br>
d. Contact Information such as email addresses and telephone numbers;<br>
in each case, in accordance with this privacy policy.<br>     

<br>
<u>Facebook Data</u> 
<br><br>
In order to register as a user with Krepta, you will be asked to sign in using your Facebook login. If you do so, you authorize us to access certain Facebook account information, such as your public Facebook profile (consistent with your privacy settings in Facebook), your email address, interests, likes, gender, birthday, education history, relationship interests, current city, photos, personal description, friend list, and information about and photos of your Facebook friends who might be common Facebook friends with other Krepta users. You will also be asked to allow Krepta to collect your location information from your device when you download or use the Service. In addition, we may collect and store any personal information you provide while using our Service or in some other manner. This may include identifying information, such as your name, address, email address and telephone number, and, if you transact business with us, financial information. You may also provide us photos, a personal description and information about your gender and preferences for recommendations, such as search distance, age range and gender. If you chat with other Krepta users, you provide us the content of your chats, and if you contact us with a customer service or other inquiry, you provide us with the content of that communication.
 <br><br>
Information Shared with Other Users. When you register as a user of Krepta, your Krepta profile will be viewable by other users of the Service. Other users (and in the case of any sharing features available on Krepta, the individuals or apps with whom a Krepta user may choose to share you with) will be able to view information you have provided to us directly or through Facebook, such as your Facebook photos, any additional photos you upload, your first name, your age, approximate number of miles away, your personal description, and information you have in common with the person viewing your profile, such as common Facebook friends and likes. If you swipe right or use voting buttons to say “yes” to a person, you will be able to chat with that person if they swipe right or use voting buttons to say “yes” to you, e.g., when you have a “match.” If you chat with your match, of course your match will see the content of your chat.
<br><br>
We do not share your personal information with others except as indicated in this Privacy Policy or when we inform you and give you an opportunity to opt out of having your personal information shared.
</p>            
<p>
5. For purposes of the Data Protection Act 1998, Krepta is the "data controller.
</p>
<p>
6. We will retain any Data you submit for 12 months.
</p>
<p>
7. Unless we are obliged or permitted by law to do so, and subject to any third party disclosures specifically set out in this policy.
</p>
<p>
8.  All personal Data is stored securely in accordance with the principles of the Data Protection Act 1998. For more details on security
see the clause below (Security).
Any or all of the above Data may be required by us from time to time in order to provide you with the best possible service and
experience when using our Website. Specifically, Data may be used by us for the following reasons:
</p>
<p>
9. <br>
a. internal record keeping;<br>
b. improvement of our products /services<br>
in each case, in accordance with this privacy policy<br>
Third party websites and services<br>
</p>
<p>
10. Krepta may, from time to time, employ the services of other parties for dealing with certain processes necessary for the operation of the Website. The providers of such services do not have access to certain personal Data provided by Users of this Website. Links to other websites responsible for the content of these websites. This privacy policy does not extend to your use of such websites. You are advised to read the privacy policy or statement of other websites prior to using them.
</p>
<p>
11. This Website may from time to time, provide links to other websites. We have no control over such websites and are not Changes of business ownership and control.
</p>
<p>
12. Krepta may from time to time expand or reduce our business and this may involve the sale and/or the transfer of control of all or part of Krepta. Data provided by Users will, where it is relevant to any part of our business so transferred, be transferred along
with that part and the new owner or newly controlling party will, under the terms of this privacy policy, be permitted to use the Data for the purposes for which it was originally supplied to us.
</p>
<p>
13. We may also disclose Data to a prospective purchaser of our business or any part of it.
</p>
<p>
14. In the above instances, we will take steps with the aim of ensuring your privacy is protected.
 </p>
<p>
<strong>Controlling use of your Data</strong>
</p>
<p>
15. Wherever you are required to submit Data, you will be given options to restrict our use of that Data. This may include the following:
</p>
<p>
16. use of Data for direct marketing purposes; and
</p>
<p>
17. sharing Data with third parties.
</p>
<p>
<strong>Functionality of the Website</strong>
 </p>
<p>
18. To use all features and functions available on the Website, you may be required to submit certain Data.
 </p>
<p>
<strong>Accessing your own Data</strong>

</p>
<p>
19. You have the right to ask for a copy of any of your personal Data held by Krepta (where such Data is held) on payment of a small
fee, which will not exceed £200.
Security                        
 </p>
<p>
20. Data security is of great importance to Krepta and to protect your Data we have put in place suitable physical, electronic and
managerial procedures to safeguard and secure Data collected via this Website.
 </p>
<p>
21. When password access is required for certain parts of the Website, you are responsible for keeping this password confidential. We endeavor to do our best to protect your personal Data. However secure and is done at your own risk. We cannot ensure the security of your Data transmitted to the Website. I, transmission of information over the internet is not entirely
Transfers outside the European Economic Area.
</p>
<p>
23. Data which we collect from you may be stored and processed in and transferred to countries outside of the European Economic
Area (EEA). For example, this could occur if our servers are located in a country outside the EEA or one of our service providers is situated in a country outside the EEA. We also share information with our group companies, some of which are located outside the
EEA. These countries may not have data protection laws equivalent to those in force in the EEA.
If we transfer Data outside the EEA in this way, we will take steps with the aim of ensuring that your privacy rights continue to be protected as outlined in this privacy policy. You expressly agree to such transfers of Data.
</p>
<p>

24 If we transfer Data ourside the EEA in this way, we will take steps with the aim of ensuring that your privacy rights continue to be protected as outlined in this privacy policy. You expressly agree to such transfers of Data.General.

 </p>
<p>
<strong>General</strong>
   </p>
<p>
25. You may not transfer any of yout rights under this privacy policy to any other person. We may transfer our rights under this privacy policy 

</p>
<p>

26. If any court or competent authority finds that any provision of this privacy policy (or part of any provision) is invalid, illegal or

 </p>
<p>
27. Unless otherwise agreed, no delay, act or omission by a party in exercising any right or remedy will be deemed a waiver of that, or
 </p>
<p>
28. This Agreement will be governed by and interpreted according to the law of England and Wales. All disputes arising under the agreement will be subject to the exclusive jurisdiction of the English and Welsh courts.
</p>
<p>
29. Krepta reserves the right to change this privacy policy as we may deem necessary from time to time or as may be required by law. Any changes will be immediately posted on the Website and you are deemed to have accepted the terms of the privacy policy on your first use of the Website following the alterations.
 </p>
<p>
You may contact Krepta by email at <a href="#">chubyilo92@gmail.com.</a>
<br>

05 August 2017

</p>

<div style="height: 100px;"></div>
       


    <!-- </img>   -->
    </body>

</html>
<style type="text/css">
div.text {
    width: 800px;
    margin: auto;
    font-size: large;
    font-family: sans-serif;
    
}


</style>