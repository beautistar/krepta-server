<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


    require_once 'vendor/autoload.php';
    
    use paragraph1\phpFCM\Client;
    use paragraph1\phpFCM\Message;
    use paragraph1\phpFCM\Recipient\Device;
    use paragraph1\phpFCM\Notification;
    
class Api extends CI_Controller  {

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
	 
	 function server_info() {
		 
		 echo phpinfo();
	 }
     
     function checkUserName($username) {
         
         $result = array();
         $username = $this->doUrlDecode($username);
         $exist = $this->api_model->checkUserName($username);
         
         if ($exist > 0) {
             $this->doRespond(201, $result);
         } else {
             $this->doRespondSuccess($result);
         }
     }
     
     function checkEmailExist($email, $token) {
         
         $result = array();
         
         $email = $this->doUrlDecode($email);
         $token = $this->doUrlDecode($token);
         
         $id_exist = $this->api_model->checkEmailExist($email);
         
         if ($id_exist == 0) {
             
             $this->doRespond(201, $result);
             return;
         } else {
             
             $this->api_model->updateToken($id_exist, $token);
             $result['user_info'] = $this->api_model->getUserInfoByEmail($email);
             $result['notification'] = $this->api_model->getNotification($id_exist);             
         }   
         
         $this->doRespondSuccess($result);         
     }
     
     function uploadImage() {
         
         $result = array();         
         
         $upload_path = "uploadfiles/files/";  

         $upload_url = base_url()."uploadfiles/files/";

        // Upload file.

        $image_name = "file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {            
            
            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;             
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {
            $this->doRespond(203, $result);// upload fail
            return;
        }         
     }  
     
     function register() {

         $result = array();
         
         $username = $this->doUrlDecode($_POST['username']);
         $name = $this->doUrlDecode($_POST['name']);
         $email = $_POST['email'];
         $age = $_POST['age'];
         $gender = $_POST['gender'];
         $latitude = $_POST['latitude'];
         $longitude = $_POST['longitude'];           
         $token = $_POST['token'];
         $photo_urls = $_POST['photo_urls'];
         
         $exist_userid = $this->api_model->checkEmail($email);
         
         if ($exist_userid > 0) {
             
             $id = $exist_userid;
             $this->api_model->updateUserProfile($id, $username, $latitude, $longitude, $token);

         } else {
            $id = $this->api_model->register($name, $username, $email, $age, $gender, $latitude, $longitude, $token);
         }                                                                                                                     
         
         $result['id'] = $id;
         $i = 0;
         foreach($photo_urls as $photo_url) {
             
             $this->api_model->saveUserPhoto($id, $i, $photo_url);
             $i++;
             
         }         
     
        $this->doRespondSuccess($result);
     }
     
     function getUsers() {
         
         $result = array();
         $id = $_POST['id'];
         $lat = $_POST['latitude'];
         $lng = $_POST['longitude'];
         $distance = $_POST['distance'];
         $start_age = $_POST['start_age'];
         $end_age = $_POST['end_age'];
         $gender = $_POST['gender'];
         $page_index = $_POST['page_index'];
         $full_swiped = $_POST['full_swiped'];
         $result['available_users'] = $this->api_model->getTotalUsers($id);
         $result['user_info'] = $this->api_model->getUsers($id, $lat, $lng, $distance, $start_age, $end_age, $gender, $page_index, $full_swiped); 
         $this->doRespondSuccess($result);        
     }
     
     
     /*
     function getUsers() {
         
         $result = array();
         $id = $_POST['id'];
         $lat = $_POST['latitude'];
         $lng = $_POST['longitude'];
         $distance = $_POST['distance'];
         $start_age = $_POST['start_age'];
         $end_age = $_POST['end_age'];
         $gender = $_POST['gender'];
         $page_index = $_POST['page_index'];
         
         $result['user_info'] = $this->api_model->getUsers($id, $lat, $lng, $distance, $start_age, $end_age, $gender, $page_index); 
         $this->doRespondSuccess($result);        
     }
     */
     
     function sendInvite() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $condition = $this->doUrlDecode($_POST['condition']);
         
         $this->api_model->setInvite($user_id, $sender_id, $condition);
         // set condition in his profile
         $this->api_model->updateCondition($user_id, $condition);
         $this->doRespondSuccess($result);
     }
     
     function replyInvite(){
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $condition = $this->doUrlDecode($_POST['condition']);
         
         $this->api_model->replyInvite($user_id, $sender_id, $condition);
         $this->doRespondSuccess($result);         
     }
     
     function getReceivedInvite($user_id) { 
         
         $result = array();
         $result['received_invite'] = $this->api_model->getReceivedInvite($user_id);
         $this->doRespondSuccess($result);
     }
     
     /****
     * This API is when user does final confirmation on user condition
     * page by clicking YES button
     * 
     * In this case, both user and sender will be saved in ready to play table and will be show eacho ther
     * Also set the invite status to 2 (confirmed invite)
     */
     
     function confirmInvite() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         
         $this->api_model->sendToPlay($user_id, $sender_id);
         $this->api_model->setAcceptInvite($user_id, $sender_id);
         $this->api_model->setRateReady($user_id, $sender_id);
         $this->sendFBPush($user_id, "You are ready to play", $sender_id, 0);
         $this->doRespondSuccess($result);         
     }       
     
     /***********
     * when usesr send picture/video from invite final confirmation
     * and when user start from ready to play user list,
     * It will disappear from ready to play and will move to chat list 
     * So that, this file will be save in chat message table
     */
     function sendFileToReadyPlay() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $file_type = $_POST['type'];//image:1, video:2
         
         
         $upload_path = "uploadfiles/files/";  

         $upload_url = base_url()."uploadfiles/files/";

        // Upload file.

        $image_name = $user_id."_file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {
            
            $this->api_model->updateReadyPlay($user_id, $sender_id);
            
            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->saveFileFromPlay($user_id, $sender_id, $file_url, $file_type);
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(203, $result);// upload fail
            return;
        }         
     }
     
     /******
     * This is when user does fianl confirmation but didn't send picture/video from invite acceptation
     * or didn't started (=  didn't clicked "START" button fron ready to play user list)
     * 
     * @param mixed $user_id
     */
     
     function getReadyPlayChatUsers($user_id) {
         
         $result = array();
         
         $result['play_users'] = $this->api_model->getReadyPlayUsers($user_id);
         $result['message_users'] = $this->api_model->getMessageUsers($user_id);
         
         $this->doRespondSuccess($result);         
     }
     
     ///////////////////////////////////////
     /* Rating part
     /*****************************************/
     
     function setRate() {
         
         $result = array();
         
         $challenge_id = $_POST['challenge_id'];
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $rate = $_POST['rate'];
         $comment = $this->doUrlDecode($_POST['comment']);
         $message = "You are rated!";
         $this->sendFBPush($user_id, $message, $sender_id, 0);
         $this->api_model->setRate($challenge_id, $user_id, $sender_id, $rate, $comment);
         $this->doRespondSuccess($result); 
     } 
     
     // for test only
     function getRateMark($id) {
         
         $result = array();
         $result['mark_avg'] = $this->api_model->getRateMark($id);
         $this->doRespondSuccess($result);
     }
     
     function getRate($user_id) { // get rating and ready to set rating/rated
         
         $result = array();
         $result['rate_users'] = $this->api_model->getRate($user_id);
         
         $this->doRespondSuccess($result);
     }
     
     //get Friends
     function searchUsers() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $search_string = $_POST['search_string'];
         
         $result['user_info'] = $this->api_model->searchFriend($user_id, $search_string);
         $this->doRespondSuccess($result);
     }
     
     function addFriend($user_id, $friend_id) {
         
         $result = array();         
         $this->api_model->addFriend($user_id, $friend_id);
         $message = "You received friend request";
         $this->sendFBPush($user_id, $message, $friend_id, 0);
         $this->doRespondSuccess($result);         
     }
     
     function acceptFriend($user_id, $friend_id) {
         
         $result = array();         
         $this->api_model->acceptFriend($user_id, $friend_id);
         $message = "Friend request is accepted";
         $this->sendFBPush($user_id, $message, $friend_id, 0);
         $this->doRespondSuccess($result);
     }
     
     function getRequestedFriends($user_id) {
         
         $result = array();         
         $result['user_info'] = $this->api_model->getRequestedFriends($user_id);
         $this->doRespondSuccess($result);
         
     }
     
     function getFriends($user_id) {
         
         $result = array();
         $result['user_info'] = $this->api_model->getFriends($user_id);
         $this->doRespondSuccess($result);
     }     
     
     function getMessageUsers($user_id) {
         
         $result = array();
         
         $result['message_users'] = $this->api_model->getMessageUsers($user_id);
         
         $this->doRespondSuccess($result);                   
     }
     
     function sendMessage() {
         
         $result = array();
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $message = $_POST['message'];
         
         $this->api_model->sendMessage($user_id, $sender_id, $message); 
         
         $this->sendFBPush($user_id, $message, $sender_id, 0);           
         
         $this->doRespondSuccess($result);          
     }
     
     function sendMessageWithFile(){
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $sender_id = $_POST['sender_id'];
         $file_type = $_POST['type'];//image:1, video:2
         
         $upload_path = "uploadfiles/files/";  

         $upload_url = base_url()."uploadfiles/files/";

        // Upload file.

        $image_name = $user_id."_file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->saveFileFromChat($user_id, $sender_id, $file_url, $file_type);
            $result['photo_url'] = $file_url;
            $this->sendFBPush($user_id, $file_url, $sender_id, $file_type);
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(203, $result);// upload fail
            return;
        }         
     }
     
     function getMessageContent($user_id, $sender_id) {
         
         $result = array();
         
         $result['message_infos'] = $this->api_model->getMessageContent($user_id, $sender_id);           
         
         $this->doRespondSuccess($result);
     }
     
     function setReadMessage($user_id, $sender_id) {
         
         $result = array();
         $this->api_model->setReadMessage($user_id, $sender_id);
         $this->doRespondSuccess($result);
     }
     
     function updateStatus() {
         
         $result = array();
         
         $id = $_POST['id'];
         $status = $_POST['status'];
         $this->api_model->updateProfile($id, $status);
         $this->doRespondSuccess($result);         
     }
     
     function updateProfile() {

         $result = array();
         
         $id = $_POST['id'];
         $photo_urls = $_POST['photo_urls'];
         
         $this->api_model->deletePhoto($id);
         $i = 0;
         foreach($photo_urls as $photo_url) {
             
             $this->api_model->saveUserPhoto($id, $i, $photo_url);
             $i++;             
         }

        $this->doRespondSuccess($result);
     } 
     
     function registerToken($user_id, $token) {
         
         $token = $this->doUrlDecode($token);
         
         $result = array();         
         $this->api_model->registerToken($user_id, $token);         
         $this->doRespondSuccess($result);
     }
     
     /**
     * send push 
     * 
     * @param mixed $user_id
     * @param mixed $body
     * @param mixed $sender
     * @param mixed $content_type  : message content type, 0:text, 1: image, 2: video
     */
     function sendFBPush($user_id, $body, $sender, $content_type) { 
     
     $response = array();    

        $this->db->where('id', $sender); 
        $row = $this->db->get('tb_user')->row();        
        
         if (count($row) == 0) {
             return;
         } 
         $token = $row->device_token;        
         $name = $this->db->where('id', $user_id)->get('tb_user')->row()->name;                  
        
        /// if token is empty, save this notification 
        $noti = $body;
        if (strlen($token) < 10) {
            if ($content_type == 1) {
                $noti = "Image received";
            } else if ($content_type == 2) {
                $noti = "Video received";
            }
            $this->api_model->saveNotification($user_id, $sender, $noti);             
        }
        $apiKey = 'AAAAwHMDSbg:APA91bGWBzidAQzgHwnKllR2opBtTkoCi939ZdlafL-uXGapKiJERYZark3PF1uQezap5Rwnfc1LWkBZ7OrGNJZistpbTfGMAz-XxWpbqpAU3Q-eD6XvjnhGLnHcOhRAN-SIIiRDNbv6';
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification('Krepta', $body);
        $note->setColor('#ffffff')
            ->setBadge(1)
            ->setSound(5);

        $message = new Message();
        $message->addRecipient(new Device($token));
        $message->setNotification($note)->setData(array('sender' => $user_id, 'sender_name' => $name, 'type' => $content_type, 'body' => $body));

        $response = $client->send($message);
                      
     }                            
}

?>
