<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
    
    function __construct(){
        
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper('url');      
        $this->load->library('session');
        $this->load->database();
        //$this->load->model('admin_model'); 
        
    }     
    
    function index(){
        
        $this->load->view("home");
    }  
    
}