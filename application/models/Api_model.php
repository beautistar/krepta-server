<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function checkUserName($username) { 

        $this->db->where('username', $username);         
        return $this->db->get('tb_user')->num_rows();
    }
    
    function checkEmailExist($email) {
        
        $this->db->where('email', $email);
        $query = $this->db->get('tb_user');         
        if ($query->num_rows() == 0 ) {
            return 0;
        } else {
            return $query->row()->id;
        }        
    }
    
    function getUserInfoByEmail($email) {
        
        $result = array();
        
        $this->db->where('email', $email);
        $query = $this->db->get('tb_user');
        
        if ($query->num_rows() > 0) {
            
            $near_user = $query->row();
            
            $photo_urls = array();
                 $this->db->where('user_id', $near_user->id);
                 $photo_query = $this->db->get('tb_user_profile');
                 foreach($photo_query->result() as $photos) {                     
                     
                        array_push($photo_urls, $photos->photo_url);                      
                 }
        
            $result = array('id' => $near_user->id,
                              'username' => $near_user->username,
                              'name' => $near_user->name,
                              'email' => $near_user->email,
                              'age' => $near_user->age,
                              'gender' => $near_user->gender,
                              'latitude' => $near_user->latitude,
                              'longitude' => $near_user->longitude,                                                             
                              'condition' => $near_user->condition,
                              'status' => $near_user->status,                              
                              'photo_url' => $photo_urls);
        }
        
        return $result;
    }
    
    function checkEmail($email) {

        $this->db->where('email', $email);         
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            $this->db->where('email', $email);
            return $this->db->get('tb_user')->row()->id;
        } else {
            return 0;
        }
    }
    
    
    function getUserPhotos($id) {
        
        $result = array();
        
        $this->db->where('user_id', $id);
        $query = $this->db->get('tb_user_profile');
        
        if ($query->num_rows() > 0 ) {
            
            $i = 0;
            foreach($query->result() as $photo) {
                
                $result[$i] = $photo->photo_url;
                $i++;
            }
        }
        
        return $result;
    }

    
    function register($name, $username, $email, $age, $gender, $latitude, $longitude, $token) {
        
        if (strlen($token) < 10 ) $token = "";

        $this->db->set('username', $username);
        $this->db->set('name', $name);
        $this->db->set('email', $email);        
        $this->db->set('age', $age);
        $this->db->set('gender', $gender);
        $this->db->set('latitude', $latitude);
        $this->db->set('longitude', $longitude);        
        $this->db->set('device_token', $token);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function updateUserProfile($id, $username, $latitude, $longitude, $token) {
        
        $this->db->where('id', $id);
        $this->db->set('username', $username);
        $this->db->set('latitude', $latitude);
        $this->db->set('longitude', $longitude);
        $this->db->set('device_token', $token);
        $this->db->update('tb_user');
    }
    
    function saveUserPhoto($user_id, $photo_id, $photo_url) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('photo_id', $photo_id);
        $this->db->set('photo_url', $photo_url);
        $this->db->insert('tb_user_profile');
        $this->db->insert_id();        
    }
    function deletePhotos($user_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->delete('tb_user_profile');
    }
    /*
    function getUsers($id, $lat, $lng, $distance, $start_age, $end_age, $gender, $page_index) {
        
        $result = array();
     
        $start =  ($page_index - 1) * 20;
        $user_query = "SELECT tb_user.*, tbl2.distance AS distance
                        
                        FROM (
                               SELECT id, distance
                               FROM ( SELECT id,
                                    DEGREES(ACOS(
                                        COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                        SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                        ) * 60 * 1.1515 * 1.609344 as distance
                                    FROM tb_user
                                    ) tb_distance                                    
                                  ";
                                  if ($distance > 0) $user_query.=" WHERE distance <= " . $distance;
                                  
                               $user_query.=") tbl2, tb_user
                        
                        WHERE tbl2.id = tb_user.id AND tb_user.id != $id";
                           
                        $gender_where = " AND tb_user.gender = '$gender'"; 
                        $age_where = " AND tb_user.age BETWEEN $start_age AND $end_age";
                        if ($start_age > 0 && $end_age < 100) $user_query .= $age_where;
                        if ($gender != 'Anyone') $user_query .= $gender_where;
                        
                        $user_query .= " GROUP BY tb_user.id
                        ORDER BY tb_user.reg_date DESC, tbl2.distance ASC 
                        LIMIT ? 
                        OFFSET ?
         ";
         $user_query_params = array($lat, $lng, $lat, 20, $start);
         $query = $this->db->query($user_query, $user_query_params);
         
         if ($query->num_rows() > 0) {
             
             foreach($query->result() as $near_user) {
                 
                 $photo_urls = array();
                 $this->db->where('user_id', $near_user->id);
                 $photo_query = $this->db->get('tb_user_profile');
                 foreach($photo_query->result() as $photos) {
                     
                     if (strlen($photos->photo_url) > 0) {
                         array_push($photo_urls, $photos->photo_url);
                     }
                 }
                 
                 $is_friend = 0;
                 
                 $arr = array('id' => $near_user->id,
                              'username' => $near_user->username,
                              'name' => $near_user->name,
                              'email' => $near_user->email,
                              'age' => $near_user->age,
                              'gender' => $near_user->gender,
                              'latitude' => $near_user->latitude,
                              'longitude' => $near_user->longitude,                              
                              'is_friend' => $is_friend,
                              'condition' => $near_user->condition,
                              'status' => $near_user->status,
                              'distance' => $near_user->distance,
                              'photo_url' => $photo_urls);
                 array_push($result, $arr);                 
                 
             }
         }
         
         return $result;         
    }
    */
    
    function getTotalUsers($id) {
        
        $cnt = $this->db->get('tb_user')->num_rows() - $this->db->where('user_id', $id)->get('tb_invite')->num_rows() - 1;
        
        if ($cnt<0) {
            $cnt=0;
        }        
        return $cnt;
    }
    
    
    function getUsers($id, $lat, $lng, $distance, $start_age, $end_age, $gender, $page_index, $full_swiped) {
        
        if ($full_swiped == 1) {
            $this->db->where('user_id', $id);
            $this->db->set('is_invited', 0);
            $this->db->update('tb_invite');
        }
        
        $result = array();
        
        $count = 10;
        
        $start =  ($page_index - 1) * $count;
        $user_query = "SELECT tb_user.*, tbl2.distance AS distance
                        FROM (
                               SELECT id, distance
                               FROM ( SELECT id,
                                    DEGREES(ACOS(
                                        COS(RADIANS(?)) * COS( RADIANS(latitude)) * COS(RADIANS(?) - RADIANS(longitude)) +
                                        SIN(RADIANS(?)) * SIN( RADIANS(latitude)) )
                                        ) * 60 * 1.1515 * 1.609344 as distance
                                    FROM tb_user
                                    ) tb_distance                                    
                                  ";
                                  if ($distance > 0) $user_query.=" WHERE distance <= " . $distance;
                                  
                               $user_query.=") tbl2,";
        if ($full_swiped == 0) {
            $user_query.=" (SELECT tb_user.* FROM tb_user WHERE NOT EXISTS (SELECT tb_invite.sender_id FROM tb_invite WHERE tb_user.id=tb_invite.sender_id AND tb_invite.user_id=$id AND tb_invite.is_invited=1)) AS tb_user";
        } else {
            $user_query .=" tb_user";
        }
        $user_query .= " WHERE tbl2.id = tb_user.id AND tb_user.id != $id";
                           
                        $gender_where = " AND tb_user.gender = '$gender'"; 
                        $age_where = " AND tb_user.age BETWEEN $start_age AND $end_age";
                        if ($start_age > 0 && $end_age < 100) $user_query .= $age_where;
                        if ($gender != 'Anyone') $user_query .= $gender_where;
                        
                        $user_query .= " GROUP BY tb_user.id
                        ORDER BY tb_user.reg_date DESC, tbl2.distance ASC 
                        LIMIT ? 
                        OFFSET ?
         ";
         $user_query_params = array($lat, $lng, $lat, $count, $start);
         $query = $this->db->query($user_query, $user_query_params);
         
         if ($query->num_rows() > 0) {
             
             foreach($query->result() as $near_user) {
                 
                 $photo_urls = array();
                 $this->db->where('user_id', $near_user->id);
                 $photo_query = $this->db->get('tb_user_profile');
                 foreach($photo_query->result() as $photos) {
                     
                     if (strlen($photos->photo_url) > 0) {
                         array_push($photo_urls, $photos->photo_url);
                     }
                 }
                 
                 $arr = array('id' => $near_user->id,
                              'username' => $near_user->username,
                              'name' => $near_user->name,
                              'email' => $near_user->email,
                              'age' => $near_user->age,
                              'gender' => $near_user->gender,
                              'latitude' => $near_user->latitude,
                              'longitude' => $near_user->longitude,
                              'condition' => $near_user->condition,
                              'status' => $near_user->status,
                              'distance' => $near_user->distance,
                              'photo_url' => $photo_urls);
                 array_push($result, $arr);                 
                 
             }
         }
         
         return $result;         
    }
    
    function getUserOne($id) {
        
        $result = array();
        
        $query = $this->db->where('id', $id)->get('tb_user');
        if ($query->num_rows() > 0) {
            
            $user = $query->row();
            
            $photo_urls = array();
            $this->db->where('user_id', $user->id);
            $photo_query = $this->db->get('tb_user_profile');            
            foreach($photo_query->result() as $photos) {
                     
                 if (strlen($photos->photo_url) > 0) {
                     array_push($photo_urls, $photos->photo_url);
                 }
             }

            $result = array('id' => $user->id,
                            'username' => $user->username,
                            'name' => $user->name,
                            'email' => $user->email,
                            'age' => $user->age,
                            'gender' => $user->gender,
                            'latitude' => $user->latitude,
                            'longitude' => $user->longitude,
                            'status' => $user->status,                            
                            'photo_url' => $photo_urls);
        }
        
        return $result;
    }
    
    function setInvite($user_id, $sender_id, $condition) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        
        if ($this->db->get('tb_invite')->num_rows() == 0) {
           $this->db->set('user_id', $user_id); 
           $this->db->set('sender_id', $sender_id); 
           $this->db->set('invite_condition', $condition);
           $this->db->set('status', 0);
           $this->db->set('is_invited', 1);
           $this->db->insert('tb_invite');
           $this->db->insert_id(); 
        } else {
            
           $this->db->where('user_id', $user_id); 
           $this->db->where('sender_id', $sender_id); 
           $this->db->set('invite_condition', $condition);
           $this->db->set('status', 0);
           $this->db->set('is_invited', 1);
           $this->db->update('tb_invite');             
        }
    }
    
    function updateCondition($user_id, $condition) {
        
        $this->db->where('id', $user_id);
        $this->db->set('condition', $condition);
        $this->db->update('tb_user');
    }
    
    function replyInvite($user_id, $sender_id, $condition) {
        
       $this->db->where('sender_id', $user_id); 
       $this->db->where('user_id', $sender_id); 
       $this->db->set('reply_condition', $condition);
       $this->db->set('status', 1);
       $this->db->update('tb_invite');
    }
    
    function getReceivedInvite($user_id) {
        
        $invite_users = array();        
        
        $sql = "SELECT * from tb_invite
                WHERE (sender_id = $user_id AND status = 0) OR (user_id = $user_id AND status = 1)";
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $invite) {
                if ($invite->status == 0) $target_id = $invite->user_id;//in case of received invitation
                else $target_id = $invite->sender_id;// in case of replied invitation by someone.= waiting final confirm by me
                $invite_user = $this->getUserOne($target_id);
                
                $arr = array('invite_user' => $invite_user,
                             'invite_condition' => $invite->invite_condition,
                             'reply_condition' => $invite->reply_condition,
                             'request_user' => $invite->user_id,
                             'status' => $invite->status);
                array_push($invite_users, $arr);  
            }
        }
        
        return $invite_users;        
    }
    
    function setRate($challenge_id, $user_id, $sender_id, $rate, $comment) {
        
        $this->db->where('challenge_id', $challenge_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        
        if ($this->db->get('tb_rate')->num_rows() > 0) {            
            $this->db->where('challenge_id', $challenge_id);
            $this->db->where('user_id', $user_id);
            $this->db->where('sender_id', $sender_id);
            $this->db->set('rate', $rate);
            $this->db->set('comment', $comment);
            $this->db->update('tb_rate');
        }
    }
    
    function getRate($user_id){//received and can give rating
    
        $result = array();
        
        $this->db->where('user_id', $user_id);        
        $this->db->or_where('sender_id', $user_id);
        $query = $this->db->get('tb_rate');
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $rate) {
                
                $rated_mark = 0;
                $rating_mark = 0;
                $is_rating = 0;
                $is_rated = 0;
                $rate_user = array();
                
                if ($user_id == $rate->sender_id) {
                    $rate_user = $this->getUserOne($rate->user_id);
                    $is_rating = 1;
                    $rating_mark = $rate->rate;
                } else {
                    $rate_user = $this->getUserOne($rate->sender_id);
                    $is_rated = 1;
                    $rated_mark =  $rate->rate;
                }
                
                $rate_user['rating_score']  = $rating_mark;
                $rate_user['rated_score']  = $rated_mark;
                $rate_user['challenge_id']  = $rate->challenge_id;
                $rate_user['is_rating'] = $is_rating;  
                $rate_user['is_rated'] = $is_rated;               
                array_push($result, $rate_user);                 
            }
        }       
             
        return $result; 
    }
    
    //function getRate($user_id){//received and can give rating
//    
//        $result = array();
//        
//        /*
//        $this->db->where('user_id', $user_id);
//        $this->db->where('status', 2);
//        $this->db->or_where('sender_id', $user_id);
//        */
//        $sql = "SELECT * from tb_invite WHERE (user_id = $user_id or sender_id = $user_id) AND status = 2";
//        
//        $query = $this->db->query($sql);
//        if ($query->num_rows() > 0) {
//            
//            foreach ($query->result() as $user) {
//                
//                $rate_user = array();
//                if ($user_id == $user->sender_id) {
//                    $rate_user = $this->getUserOne($user->user_id);
//                } else {
//                    $rate_user = $this->getUserOne($user->sender_id);
//                }
//                
//                $rated_mark = 0;
//                $rating_mark = 0;
//                $is_rating = 0;
//                $this->db->where('sender_id', $user_id);
//                $this->db->where('user_id', $rate_user['id']);
//                $r_query = $this->db->get('tb_rate');
//                if($r_query->num_rows() > 0) {
//                    $is_rating = 1;
//                    $rating_mark = $r_query->row()->rate;
//                }
//                
//                $is_rated = 0;
//                $this->db->where('sender_id', $rate_user['id']);
//                $this->db->where('user_id', $user_id);
//                $r_query = $this->db->get('tb_rate');
//                if($r_query->num_rows() > 0) {
//                    $is_rated = 1;
//                    $rated_mark =  $r_query->row()->rate;
//                }
//                
//                $rate_user['rating_score']  = $rating_mark;
//                $rate_user['rated_score']  = $rated_mark;
//                $rate_user['is_rating'] = $is_rating;  
//                $rate_user['is_rated'] = $is_rated;               
//                array_push($result, $rate_user);                 
//            }
//        }       
//             
//        return $result; 
//    }

    
    
    // for test
//    function getRateMark($user_id) {
//        
//        $this->db->select_avg('rate');
//        $this->db->where('sender_id', $user_id);
//        return $this->db->get('tb_rate')->row(); 
        //"rate": "3.95000"       
//    }
    
    function setAcceptInvite($user_id, $sender_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        $this->db->set('status', 2);
        $this->db->update('tb_invite');
    }
    
    function setRateReady($user_id, $sender_id) {
        
        $challenge_id = 1;
        
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        $query = $this->db->get('tb_rate');
        if ($query->num_rows() > 0) {
            $challenge_id = $query->row()->challenge_id + 1;
        }
        
        $this->db->set('user_id', $user_id);
        $this->db->set('sender_id', $sender_id);
        $this->db->set('challenge_id', $challenge_id);
        $this->db->set('rate', 0);
        $this->db->set('comment', "");
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_rate');
        $this->db->insert_id();  
        
        $this->db->set('sender_id', $user_id);
        $this->db->set('user_id', $sender_id);
        $this->db->set('challenge_id', $challenge_id);
        $this->db->set('rate', 0);
        $this->db->set('comment', "");
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_rate');
        $this->db->insert_id(); 
    }
    
    function sendToPlay($user_id, $sender_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        
        if ($this->db->get('tb_play')->num_rows() == 0) {        
        
            $this->db->set('user_id', $user_id);
            $this->db->set('sender_id', $sender_id);
            $this->db->set('status', 0);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->insert('tb_play');
            $this->db->insert_id();  
        }
        
        $this->db->where('user_id', $sender_id);
        $this->db->where('sender_id', $user_id);
        
        if ($this->db->get('tb_play')->num_rows() == 0) { 
        
            $this->db->set('user_id', $sender_id);
            $this->db->set('sender_id', $user_id);
            $this->db->set('status', 0);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->insert('tb_play');
            $this->db->insert_id();
        }       
    }
    
    function updateReadyPlay($user_id, $sender_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('sender_id', $sender_id);
        $this->db->set('status', 1);
        $this->db->update('tb_play');         
    }
    
    function saveFileFromPlay($user_id, $sender_id, $file_url, $type) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('sender_id', $sender_id);
        $this->db->set('message', $file_url);
        $this->db->set('type', $type);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_message');
        $this->db->insert_id();
    }
    
    function saveFileFromChat($user_id, $sender_id, $file_url, $type) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('sender_id', $sender_id);
        $this->db->set('message', $file_url);
        $this->db->set('type', $type);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_message');
        $this->db->insert_id();
    }
    
    function getReadyPlayUsers($user_id) {
        
        $result = array();
        
        $sql = "SELECT A.*, B.sender_id AS sender_id 
                FROM tb_invite AS A 
                LEFT JOIN tb_play AS B 
                ON (B.user_id = A.user_id AND B.sender_id = A.sender_id) 
                OR (B.user_id = A.sender_id AND B.sender_id = A.user_id) 
                WHERE B.status = 0 AND A.status = 2 AND B.user_id = $user_id";
        $query = $this->db->query($sql); 
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $invite) {
                
                $invite_user = $this->getUserOne($invite->sender_id);
                
                $arr = array('invite_user' => $invite_user,
                             'invite_condition' => $invite->invite_condition,
                             'reply_condition' => $invite->reply_condition,
                             'request_user' => $invite->user_id,
                             'status' => $invite->status);
                array_push($result, $arr); 
                
            }
        }
        
        return $result;
    }
    
    /// serch part /////////
    function searchFriend($user_id, $search_string) {
        
        $result = array();
        
        $this->db->where('id !=', $user_id);
        $this->db->like('username', $search_string);
        $query = $this->db->get('tb_user');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $near_user) {
                
                $photo_urls = array();
                 $this->db->where('user_id', $near_user->id);
                 $photo_query = $this->db->get('tb_user_profile');
                 foreach($photo_query->result() as $photos) {
                     
                     if (strlen($photos->photo_url) > 0) {
                         array_push($photo_urls, $photos->photo_url);
                     }
                 }
                 
                 $arr = array('id' => $near_user->id,
                              'username' => $near_user->username,
                              'name' => $near_user->name,
                              'email' => $near_user->email,
                              'age' => $near_user->age,
                              'gender' => $near_user->gender,
                              'latitude' => $near_user->latitude,
                              'longitude' => $near_user->longitude,
                              'condition' => $near_user->condition, 
                              'status' => $near_user->status,                             
                              'photo_url' => $photo_urls);
                 array_push($result, $arr); 
            }
        }
        
        return $result;
    }
    
    function addFriend($user_id, $friend_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('friend_id', $friend_id);
        if ($this->db->get('tb_friend')->num_rows() == 0) {
            
            $this->db->set('user_id', $user_id);
            $this->db->set('friend_id', $friend_id);
            $this->db->set('status', 0);
            $this->db->insert('tb_friend');
            $this->db->insert_id();            
        }
    }
    
    function acceptFriend($user_id, $friend_id) {
        
        $this->db->where('user_id', $friend_id);
        $this->db->where('friend_id', $user_id);
        $this->db->set('status', 1);
        $this->db->update('tb_friend'); 
        
        $this->db->where('user_id', $user_id);
        $this->db->where('friend_id', $friend_id);
        $query = $this->db->get('tb_friend');
        
        if ($query->num_rows() > 0) {            
            $this->db->where('user_id', $user_id);
            $this->db->where('friend_id', $friend_id);
            $this->db->set('status', 1);
            $this->db->update('tb_friend');            
        } else {
            $this->db->set('user_id', $user_id);
            $this->db->set('friend_id', $friend_id);
            $this->db->set('status', 1);
            $this->db->insert('tb_friend');
            $this->db->insert_id();
        }
    }
    
    function getRequestedFriends($user_id) {
        
        $result = array();
        
        $this->db->where('friend_id', $user_id);
        $this->db->where('status', 0);
       
        $query = $this->db->get('tb_friend');
        
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $friend) {
                
                $user = $this->getUserOne($friend->user_id);                 
                array_push($result, $user);                
            }
        }
        
        return $result;                                          
    }
    
    function getFriends($user_id) {
        
        $result = array();
        
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 1);
        $query = $this->db->get('tb_friend');
        
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $friend) {
                
                $user = $this->getUserOne($friend->friend_id);                 
                array_push($result, $user);                
            }
        }
        
        return $result;                                          
    }
    
    function getMessageUsers($user_id) {
        
        $result = array();
        
        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $this->db->or_where('sender_id', $user_id);
        $this->db->order_by('reg_date', 'DESC');
        $query = $this->db->get('tb_message');
        
        if ($query->num_rows() > 0) {
            
            $target_ids = array();
            $i = 0;
            foreach($query->result() as $message) {
                
                if ($message->user_id == $user_id) {
                    
                    $type = 'sent';
                    $target_id = $message->sender_id;                    
                }
                else if ($message->sender_id == $user_id) {
                    $type = 'received';
                    $target_id = $message->user_id;
                }
                
                if (in_array($target_id, $target_ids))  {
                    
                    continue;
                }
                
                $target_ids[$i] = $target_id;
                $i++;                
                
                $chat_user = $this->getUserOne($target_id);
                
                $arr = array('chat_user' => $chat_user,
                             'message' => $message->message,
                             'time' => $message->reg_date,
                             'message_type' => $type,
                             'content_type' => $message->type,
                             'read_status' => $message->read_status);
                array_push($result, $arr);
            }
        }
        
        return $result;        
    }
    
    function sendMessage($user_id, $sender_id, $message) {        
        
        $this->db->set('user_id', $user_id);
        $this->db->set('sender_id', $sender_id);
        $this->db->set('type', 0);
        $this->db->set('message', $message);
        $this->db->set('reg_date', 'NOW()', false);
        
        $this->db->insert('tb_message');
        $this->db->insert_id();
    }
    
    function getMessageContent($user_id, $sender_id) {
        
        $result = array();
        
        $this->db->where("((user_id = '$user_id' AND sender_id = '$sender_id') OR (user_id = '$sender_id' AND sender_id = '$user_id'))", NULL, FALSE);
        $this->db->order_by('reg_date', 'ASC');
        $query = $this->db->get('tb_message');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $message) {
                
                if ($message->user_id == $user_id) $type = 'sent';
                else $type = 'received';
                
                $arr = array('content' => $message->message,
                             'reg_date' => $message->reg_date,
                             'message_type' => $type,
                             'content_type' => $message->type);
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    
    function setReadMessage($user_id, $sender_id) {
        
        $this->db->where('user_id', $sender_id);
        $this->db->where('sender_id', $user_id);
        $this->db->set('read_status', 1);
        $this->db->update('tb_message');
    }
    
    function updateProfile($id, $status) {
        
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        $this->db->update('tb_user');
    }
    
    function deletePhoto($id) {
        
        $this->db->where('user_id', $id);
        $this->db->delete('tb_user_profile');
        
    }
    
    function registerToken($user_id, $token) {
        
        if (strlen($token) < 10 ) $token = "";
        $this->db->where('id', $user_id);
        $this->db->set('device_token', $token);
        $this->db->update('tb_user');                            
    }
    
    function updateToken($id, $token) {
       
        if (strlen($token) < 10 ) $token = "";
        $this->db->where('id', $id);
        $this->db->set('device_token', $token);
        $this->db->update('tb_user');                            
    }
    
    function saveNotification($user_id, $sender, $body) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('sender_id', $sender);
        $this->db->set('content', $body);
        $this->db->set('read_status', 0);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_notification');
        $this->db->insert_id();
    } 
    
    function getNotification($id) {
        
        $result = array();
        
        $this->db->where('sender_id', $id);
        $this->db->where('read_status', 0);
        $this->db->order_by('reg_date', 'DESC');
        $query = $this->db->get('tb_notification');
        
        if ($query->num_rows() > 0 ) {
            
            foreach($query->result() as $noti) {
                
                $arr = array('id' => $noti->id,
                             'content' => $noti->content);
                array_push($result, $arr);
            }    
        }
        
        $this->db->where('sender_id', $id);
        $this->db->set('read_status', 1);
        $this->db->update('tb_notification');
        
        return $result;
    }                                 
}
?>
